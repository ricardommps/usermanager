import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import TableUsers from '../_containers/TableUsers';
import { userActions } from '../_actions';

import { withStyles } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import HeaderBar  from '../_containers/Header';
import MiniDrawer from '../_containers/MiniDrawer';
import UserRegistration from '../_containers/UserRegistration'

const styles = theme => ({
    root: {
        display: 'flex',
      },
      content: {
        flexGrow: 1,
        marginTop: '65px'
      },
      adminPanel: {
          display:'flex'
      },
      title : {
          width: '90%'
      },
      panel: {
        width: '100%',
        display: 'flex',
        minHeight: '64px',
        position: 'relative',
        alignItems: 'center'
      }
  });


class HomePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openFormUser : false
        }
    }
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    logout = () => {
        const { dispatch } = this.props;
        dispatch(userActions.logout())
        this.props.dispatch(userActions.getAll());
    }

    handleCloseFormUser = () => {
        this.setState({ openFormUser: false });
    }

    handleOpenFormUser = () => {
        this.setState({ openFormUser: true });
    }

    handleSubmit = (data) => {
       if(data){
            const { dispatch } = this.props;
            if(data.id){
                dispatch(userActions.updateUser(data))
            }else{
                dispatch(userActions.registerUser(data))
            }
            this.setState({ openFormUser: false });
        } 
    }

    handleClickEdit = (id) => {
        if(id){
            const { dispatch } = this.props;
            dispatch(userActions.getById(id))
            this.setState({ openFormUser: true });
        }
    }

    handleBlock = (user) => {
        if(user){
            user.ativo = user.ativo ? false : true ;
            if(user.tableData){
                delete user.tableData;
            }
           
           const { dispatch } = this.props;
            dispatch(userActions.updateUser(user))
            this.props.dispatch(userActions.getAll());
        }
    }

    onExited = () => {
        this.props.dispatch(userActions.getAll());
    }

    render() {
        const { user, users } = this.props;
        const {classes} = this.props;
        let isAdmin = false
        if(user){
            isAdmin = user.tipoUsuario == "Administrador" ? true : false;
        }
        
        return (
            <div className={classes.root}>
                <div>
                    <HeaderBar currentUser={user} logout={this.logout}/> <MiniDrawer/>
                </div>
                <main className={classes.content}>
                    {isAdmin ?  
                    <Paper className={classes.adminPanel}>
                        <div className={classes.panel}>
                            <Typography component="p" className={classes.title}>
                                Usuarios
                            </Typography>
                            <Button variant="contained" color="primary" size="small" onClick={this.handleOpenFormUser}>
                                Cadastrar
                            </Button>
                        </div>
                    </Paper> : null}
                     {users ? <TableUsers users={users.users} handleClickEdit={this.handleClickEdit} handleBlock={this.handleBlock} isAdmin={isAdmin}/> : null}   
                    
                </main>  
               <UserRegistration 
                    open={this.state.openFormUser} 
                    onExited={this.onExited}
                    handleClose={this.handleCloseFormUser} 
                    handleSubmit={this.handleSubmit} 
                    user={users.user}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication,userById } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(withStyles(styles, { withTheme: true}) (HomePage));
export { connectedHomePage as HomePage };
