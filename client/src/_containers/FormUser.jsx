import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import NativeSelect from '@material-ui/core/NativeSelect';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
export const FormUser = props => {
    const {
      values: { tipoUsuario, ativo, nome, sobrenome, email, password },
      errors,
      touched,
      handleSubmit,
      handleChange,
      isValid,
      setFieldTouched,
      setFieldValue
    } = props;

    const change = (name, e) => {
      e.persist();
      handleChange(e);
      setFieldTouched(name, true, false);
    };

    const formControlStyle = {
      marginRight:'50px'
    }
    console.log(props)
    return (
      <form onSubmit={handleSubmit}>
        <FormControl style={formControlStyle}>
          <InputLabel shrink>
            Tipo de usuario
          </InputLabel>
          <NativeSelect
            value={tipoUsuario}
            onChange={handleChange}
            input={<Input name="tipoUsuario"/>}
          >
            <option value="" label="Selecione" />
            <option value="Administrador" label="Administrador" />
            <option value="Usuário padrão" label="Usuário padrão" />
          </NativeSelect>
        </FormControl>
    
        <TextField
          name="nome"
          helperText={touched.nome ? errors.nome : ""}
          error={Boolean(errors.nome)}
          label="Nome"
          value={nome}
          onChange={handleChange}
          fullWidth
          margin="normal"
        />
        <div>{errors.nome}</div>
        <TextField
          name="sobrenome"
          helperText={touched.sobrenome ? errors.sobrenome : ""}
          error={Boolean(errors.sobrenome)}
          label="Sobrenome"
          value={sobrenome}
          onChange={handleChange}
          fullWidth
          margin="normal"
        />
        <div>{errors.sobrenome}</div>
        <TextField
          name="email"
          helperText={touched.email ? errors.email : ""}
          error={Boolean(errors.email)}
          label="Email"
          fullWidth
          value={email}
          onChange={handleChange}
          margin="normal"
        />
        <div>{errors.email}</div>
        <TextField
          name="password"
          helperText={touched.password ? errors.password : ""}
          error={Boolean(errors.password)}
          label="Password"
          fullWidth
          type="password"
          value={password}
          onChange={handleChange}
          margin="normal"
        />
        <div>{errors.password}</div>
        <div>
          <Button
            type="submit"
            color="primary"
            disabled={!isValid}
          >
            Salvar
          </Button>
          <Button
            color="primary"
          >
            Cancelar
          </Button>
        </div>
      </form>
    );
  };
  