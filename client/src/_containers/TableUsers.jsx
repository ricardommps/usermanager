import React from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Block from '@material-ui/icons/Block';
import AddCircle from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import MaterialTable from 'material-table'
import Tooltip from '@material-ui/core/Tooltip';

export default function TableUsers (props) {
    const {users, handleClickEdit,handleBlock, isAdmin} = props
    return (
      <Paper>
        <MaterialTable
          columns={[
              { title: 'Nome', field: 'nome' },
              { title: 'Sobrenome', field: 'sobrenome' },
              { title: 'Tipo de usuário', field: 'tipoUsuario' },
              {
                  title: 'Actions',
                  field: '_id',
                  render: rowData => {
                    if(isAdmin) {
                      return (
                          <div>
                              <IconButton onClick={() => handleClickEdit(rowData.id)}>
                                  <EditIcon fontSize="small" />
                              </IconButton>
                              <Tooltip title={rowData.ativo ? "Bloquear usuario" : "Desbloquear usurio"}>
                                <IconButton onClick={() => handleBlock(rowData)}>
                                  {rowData.ativo ? 
                                    <Block fontSize="small" /> : <AddCircle fontSize="small" /> }
                                </IconButton>
                              </Tooltip>
                          </div>

                      )
                    }else{
                      return null
                    }
                      
                  },
              }
          ]}
          data={users}
          title="Usuarios"
        />
      </Paper>
    );
  }