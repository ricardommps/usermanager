import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik } from "formik";
import { FormUser } from "./FormUser"
import * as Yup from "yup";

const validationSchema = Yup.object({
    tipoUsuario: Yup.string("Selecione").required("Tipo obrigatorio"),
    nome: Yup.string("Digite o nome").required("Nome obrigatorio"),
    sobrenome: Yup.string("Digite o Sobrenome").required("Sobrenome obrigatorio"),
    email: Yup.string("Digite o email")
      .email("Entre com um email valido")
      .required("Email obrigatorio"),
    password: Yup.string("Digite sua senha")
      .min(3, "A senha deve conter pelo menos 3 caracteres")
      .required("Senha obrigatorio")
  });

export default function UserRegistration(props) {
    const { open, handleClose, handleSubmit, user, onExited } = props;
    const defaultValues  = { ativo: true, nome: "", sobrenome:"", email: "", password: "", tipoUsuario: "Administrador", };  
    const values = user ? user : defaultValues
    const fullWidth = true;
    const maxWidth = 'sm'
    return (
        <div>
             <Dialog open={open} 
                onClose={handleClose} 
                onExited={onExited}
                aria-labelledby="form-dialog-title" 
                fullWidth={fullWidth}
                maxWidth={maxWidth}>
                <DialogTitle id="form-dialog-title">Cadastrar usuário</DialogTitle>
                <DialogContent>
                <Formik
                    render={props => <FormUser {...props} />}
                    initialValues={values}
                    enableReinitialize
                    validationSchema={validationSchema}
                    onSubmit={handleSubmit}
                    />
                </DialogContent>
            </Dialog>
        </div>
    )
}

