import { userConstants } from '../_constants';

export function users(state = {}, action) {
  switch (action.type) {
    case userConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETALL_SUCCESS:
      return {
        users: action.users,
        user: null
      };
    case userConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
      case userConstants.GET_BY_ID_REQUEST:
        return {
          loading: true
        };
      case userConstants.GET_BY_ID_SUCCESS:
        return {
          user: action.users
        };
      case userConstants.GET_BY_ID_FAILURE:
        return { 
          error: action.error
        };
    case userConstants.REGISTER_USER_REQUESTE:
    case userConstants.UPDATE_USER_REQUESTE:
      return {
        loading: true
      };
    case userConstants.REGISTER_USER_SUCCESS:
    case userConstants.UPDATE_USER_SUCCESS:
      return {
        item: action.user
      };
    case userConstants.REGISTER_USER_FAILURE:
    case userConstants.UPDATE_USER_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}