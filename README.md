

##  How to install

```bash
# Install frontend dependencies
cd client
npm install

# Install backend dependencies
cd server
npm install
```

## How to run
Ensure you have jsonserver installed in your system and that it is running

### Start the backend server
Start the backend server first:

```bash
cd server
npm start
```
This will run the backend server at localhost:3030. If all is working well, you should be able to access the url http://localhost:3000/ from your Browser or Postman

### Start the client
Open a separate terminal to start the client:

```bash
cd client
npm start
```
