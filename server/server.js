const fs = require('fs')
const bodyParser = require('body-parser')
const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')

const server = jsonServer.create()
const router = jsonServer.router('./users.json')
const userdb = JSON.parse(fs.readFileSync('./users.json', 'UTF-8'))

//server.use(bodyParser.urlencoded({extended: true}))
//server.use(bodyParser.json())
server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())

server.use(jsonServer.defaults());

const SECRET_KEY = '123456789'

const expiresIn = '1h'

// Create a token from a payload 
function createToken(payload){
  return jwt.sign(payload, SECRET_KEY, {expiresIn})
}

// Verify the token 
function verifyToken(token){
  return  jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
}

// Check if the user exists in database
function isAuthenticated({email, password}){
  let index = userdb.users.findIndex(user => user.email === email && user.password === password)
  console.log(userdb.users[index])
  return userdb.users.findIndex(user => user.email === email && user.password === password) !== -1
}

function getCurrentUser(email, password) {
  let index = userdb.users.findIndex(user => user.email === email && user.password === password)
  return index >= 0 ? userdb.users[index] : null
}


server.post('/auth/login', (req, res) => {
  console.log(req.body);
  const {email, password} = req.body
  console.log(email, password)
  if (isAuthenticated({email, password}) === false) {
    const status = 401
    const message = 'Incorrect email or password'
    res.status(status).json({status, message})
    return
  }
  const access_token = createToken({email, password})
  const currentUser = getCurrentUser(email, password)
  if(currentUser) {
    if(!currentUser.ativo){
      const status = 401
      const message = 'Usuario Inativo'
      res.status(status).json({status, message})
      return
    }
    res.status(200).json({
      id: currentUser.id,
      nome: currentUser.nome,
      sobrenome: currentUser.sobrenome,
      tipoUsuario: currentUser.tipoUsuario,
      email: currentUser.email,
      token: access_token
    })
  }else{
    const status = 401
    const message = 'Incorrect email or password'
    res.status(status).json({status, message})
  }
  
})

server.use(/^(?!\/auth).*$/,  (req, res, next) => {
  console.log("------");
  if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
    const status = 401
    const message = 'Error in authorization format'
    res.status(status).json({status, message})
    return
  }
  try {
     verifyToken(req.headers.authorization.split(' ')[1])
     next()
  } catch (err) {
    const status = 401
    const message = 'Error access_token is revoked'
    res.status(status).json({status, message})
  }
})

server.use(router)

server.listen(3000, () => {
  console.log('Run Auth API Server')
})